var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var db = mongojs('mongodb://cooper:cooper1@ds033113.mlab.com:33113/mytasklist_cooper', ['tasks']);

// Get all tasks
router.get('/tasks', function (req, res, next) {
    db.tasks.find(function (err, tasks) {
        if (!err) {
            res.json(tasks);
        } else {
            res.status(400).send(err);
        }
    });
});

// Get single tasks
router.get('/tasks/:id', function (req, res, next) {
    db.tasks.findOne({ _id: mongojs.ObjectId(req.params.id) }, function (err, task) {
        if (!err) {
            res.json(task);
        } else {
            res.status(400).send(err);
        }
    });
});

// Save task
router.post('/task', function (req, res, next) {
    var task = req.body;
    if (!task.title || !(task.isDone + '')) {
        res.status(400).json({ error: "Bad data" });
    } else {
        db.tasks.save(task, function (err, task) {
            if (!err) {
                res.json(task);
            } else {
                res.status(400).send(err);
            }
        });
    }
});

// Delete task
router.delete('/task/:id', function (req, res, next) {
    db.tasks.remove({ _id: mongojs.ObjectId(req.params.id) }, function (err, task) {
        if (!err) {
            res.json(task);
        } else {
            res.status(400).send(err);
        }
    });
});

// Update task
router.put('/task/:id', function (req, res, next) {
    var task = req.body;
    var updTask = {};

    if (task.isDone) {
        updTask.isDone = task.isDone;
    }
    if (task.title) {
        updTask.title = task.title;
    }

    if (!updTask) {
        res.status(400).json({ error: "Bad data" });
    } else {
        db.tasks.update({ _id: mongojs.ObjectId(req.params.id) }, updTask, function (err, task) {
            if (!err) {
                res.json(task);
            } else {
                res.status(400).send(err);
            }
        });
    }
});

module.exports = router;